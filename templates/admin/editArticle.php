<?php include "templates/include/header.php" ?>
    
  <div class="editArticle">
    <div class="container">

      <div class="adminHeader" id="adminHeader">
        <a class="adminHeader__logout" href="admin.php?action=logout"?>Log out</a>
        <h1 class="adminHeader__title">VM News Admin</h1>
        <p class="adminHeader__username">You are logged in as <b><?php echo htmlspecialchars( $_SESSION['username']) ?></b>.</p>
      </div>

      <h1><?php echo $results['pageTitle']?></h1>

      <form class="addArticle-form" action="admin.php?action=<?php echo $results['formAction']?>" method="post">
        <input type="hidden" name="articleId" value="<?php echo $results['article']->id ?>"/>

<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>

        <ul class="adminHeader__list">

          <li class="adminHeader__item">
            <label class="adminHeader__label" for="title">Article Title</label>
            <input class="adminHeader__input" type="text" name="title" id="title" placeholder="Name of the article" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['article']->title )?>" />
          </li>

          <li class="adminHeader__item">
            <label class="adminHeader__label" for="summary">Article Summary</label>
            <textarea class="adminHeader__content" name="summary" id="summary" placeholder="Brief description of the article" required maxlength="1000" style="height: 5em;"><?php echo htmlspecialchars( $results['article']->summary )?></textarea>
          </li>

          <li class="adminHeader__item">
            <label class="adminHeader__label" for="content">Article Content</label>
            <textarea class="adminHeader__content" name="content" id="content" placeholder="The HTML content of the article" required maxlength="100000" style="height: 30em;"><?php echo htmlspecialchars( $results['article']->content )?></textarea>
          </li>

          <li class="adminHeader__item">
            <label class="adminHeader__label" for="publicationDate">Publication Date</label>
            <input class="adminHeader__input" type="date" name="publicationDate" id="publicationDate" placeholder="YYYY-MM-DD" required maxlength="10" value="<?php echo $results['article']->publicationDate ? date( "Y-m-d", $results['article']->publicationDate ) : "" ?>" />
          </li>


        </ul>

        <div class="buttons">
          <input class="btn btn-submit" type="submit" name="saveChanges" value="Save Changes" />
          <input class="btn btn-submit" type="submit" formnovalidate name="cancel" value="Cancel" />
        </div>

      </form>

<?php if ( $results['article']->id ) { ?>
      <p class="homepage mt-6"><a class="archive-link" href="admin.php?action=deleteArticle&amp;articleId=<?php echo $results['article']->id ?>" onclick="return confirm('Delete This Article?')">Delete This Article</a></p>
<?php } ?>

    </div>
  </div>
<?php include "templates/include/footer.php" ?>

<!-- 
  1. Он публикует сообщения либо admin.php?action=newArticle, либо admin.php?action=editArticle, в зависимости от значения, переданного в переменной $results['formAction']. 

  2. Он также содержит скрытое поле articleId для отслеживания идентификатора редактируемой статьи (если таковая имеется).

  3. Форма также содержит область для сообщений об ошибках, а также поля для заголовка статьи, резюме, содержания и даты публикации.

  4. Наконец, есть 2 кнопки для сохранения и отмены изменений, а также ссылка, позволяющая администратору удалить отредактированную в данный момент статью.
  
  5. мы передаем все данные через htmlspecialchars (), прежде чем вывести их в разметку. Это не только хорошая привычка безопасности, но и гарантирует, что наши значения полей формы правильно экранируются. 

  Мы передаем все данные через htmlspecialchars (), прежде чем вывести их в разметку. Это не только хорошая привычка безопасности, но и гарантирует, что наши значения полей формы правильно экранируются. 

  6. На использование атрибута HTML5 formnovalidate на кнопке “отмена”. Этот удобный атрибут говорит браузеру не проверять форму, если пользователь нажимает кнопку “Отмена”.
 -->