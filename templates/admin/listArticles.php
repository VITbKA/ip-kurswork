<?php include "templates/include/header.php" ?>
    
  <div class="listArticles">
    <div class="container">

      <div class="adminHeader" id="adminHeader">
        <a class="adminHeader__logout" href="admin.php?action=logout"?>Log out</a>
        <h1 class="adminHeader__title">VM News Admin</h1>
        <p class="adminHeader__username">You are logged in as <b><?php echo htmlspecialchars( $_SESSION['username']) ?></b>.</p>
      </div>

<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>


<?php if ( isset( $results['statusMessage'] ) ) { ?>
        <div class="statusMessage"><?php echo $results['statusMessage'] ?></div>
<?php } ?>

      <table>
        <caption>All Articles</caption>
        <tr>
          <th>Publication Date</th>
          <th>Article</th>
        </tr>

<?php foreach ( $results['articles'] as $article ) { ?>

        <tr onclick="location='admin.php?action=editArticle&amp;articleId=<?php echo $article->id?>'">
          <td><?php echo date('j M Y', $article->publicationDate)?></td>
          <td>
            <?php echo $article->title?>
          </td>
        </tr>

<?php } ?>

      </table>

      <div class="operation">
        <p class="article__total"><span>[ <?php echo $results['totalRows']?> article<?php echo ( $results['totalRows'] != 1 ) ? 's' : '' ?> ]</span> in total.</p>

        <p class="operation__add"><a class="archive-link" href="admin.php?action=newArticle">Add a New Article</a></p>
      </div>
      <p class="homepage mt-6"><a class="archive-link" href="./">Return to Homepage</a></p>
    </div>
  </div>
<?php include "templates/include/footer.php" ?>

<!-- 
  1. После отображения любых сообщений об ошибках или состоянии он циклически просматривает массив объектов статьи, хранящихся в $results['articles'], отображая дату публикации и название каждой статьи в строке таблицы.

  2. Он также добавляет событие onclick JavaScript в строку таблицы каждой статьи, чтобы администратор мог щелкнуть Статью для ее редактирования.
  
  3. Шаблон также включает в себя общее количество статей, а также ссылку, позволяющую администратору добавить новую статью.
 -->