<?php

/**
 * 1. Вызывает отображение ошибок в браузере
 * 2. Установливаем часовой пояс
 Поскольку наша CMS будет использовать функцию PHP date (), нам нужно сообщить PHP часовой пояс нашего сервера (в противном случае PHP генерирует предупреждающее сообщение).
 
 * 3. Установливаем сведения о доступе к базе данных
 DB_DSN: сообщает PHP о том, где найти нашу БД

 * 4. Устанавливаем логин и пароль для пользователя БД 
 * 5. Устанавливаем пути
 CLASS_PATH, который является путем к файлам классов
 TEMPLATE_PATH, где наш скрипт должен искать файлы шаблонов HTML

 * 6. Указываем число статей, показывающихся на домашней странице
 * 7. Устанавливаем логин и пароль для админки сайта
 * 8. Подключаем класс Article 
 Файл класса Article необходим всем скриптам в нашем приложении

 * 9. Создаем обработчик исключений 
 Повышает безопасность, обрабатывая любые исключения PDO, которые в противном случае могли бы отображать имя пользователя и пароль базы данных на странице.

 Как только мы определили handleException(), мы устанавливаем его в качестве обработчика исключений, вызывая функцию PHP set_exception_handler ().

**/

ini_set( "display_errors", true );							// 1
date_default_timezone_set( "Europe/Kiev" );					// 2
define( "DB_DSN", "mysql:host=localhost;dbname=cms" );		// 3
define( "DB_USERNAME", "root" );							// 4
define( "DB_PASSWORD", "root" );
define( "CLASS_PATH", "classes" );							// 5
define( "TEMPLATE_PATH", "templates" );
define( "HOMEPAGE_NUM_ARTICLES", 5 );						// 6
define( "ADMIN_USERNAME", "admin" );						// 7
define( "ADMIN_PASSWORD", "mypass" );
require( CLASS_PATH . "/Article.php" );						// 8

// 9
function handleException( $exception ) {
  echo "Sorry, a problem occurred. Please try later.";
  error_log( $exception->getMessage() );
}

set_exception_handler( 'handleException' );
?>
