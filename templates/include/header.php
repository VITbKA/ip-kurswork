<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php echo htmlspecialchars( $results['pageTitle'] )?></title>
  <link rel="stylesheet" href="style.css" />
</head>

<body>
<div class="page">
  <header class="header">
    <div class="container" id="container">

      <div class="header__inner">
        <a class="header__link" href=".">
          <img class="img header__img" id="logo" src="images/logo_little.svg" alt="VM News" />
        </a>

        <a class="header__link" href="admin.php">Site Admin</a>
      </div>
    </div>
  </header>

<!-- 
  1. Этот код просто отображает разметку для запуска HTML-страницы.
  2. Он использует переменную $results['pageTitle'], переданную из основного скрипта (index.php или admin.php) для установки элемента title, а также ссылки на таблицу стилей style.css.
  3. Мы передали значение $results['pageTitle'] через PHP - функцию htmlspecialchars(). Эта функция кодирует любые специальные HTML-символы, такие как<, > и&, в их эквиваленты HTML-сущностей (&lt;, &gt; и &amp;).
  4. Наряду с фильтрацией ввода — что мы и сделали, когда писали конструктор статьи — кодирование вывода является хорошей привычкой безопасности, чтобы войти в нее. Мы будем кодировать большинство данных в наших шаблонах таким образом.
 -->
      