<?php include "templates/include/header.php" ?>
    
    <div class="container">
      <form class="adminForm" action="admin.php?action=login" method="post">
        <input type="hidden" name="login" value="true" />

<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>

        <ul class="adminForm__list">

          <li class="adminForm__item">
            <label class="adminForm__label" for="username">Username</label>
            <input class="adminForm__input" type="text" name="username" id="username" placeholder="Your admin username" required autofocus maxlength="20" />
          </li>

          <li class="adminForm__item">
            <label class="adminForm__label" for="password">Password</label>
            <input class="adminForm__input" type="password" name="password" id="password" placeholder="Your admin password" required maxlength="20" />
          </li>

        </ul>

        <div class="buttons">
          <input class="btn btn-submit" type="submit" name="login" value="Login" />
        </div>

      </form>

      <p class="homepage mt-6"><a class="archive-link" href="./">Return to Homepage</a></p>

    </div>

<?php include "templates/include/footer.php" ?>

<!-- 
  1. Эта страница содержит форму входа администратора, которая отправляет сообщение обратно в admin.php?action=login. 

  2. Он включает в себя скрытое поле login, которое наша функция login() из admin.php использует для проверки того, была ли форма опубликована.
  
  3. Форма также содержит область для отображения любых сообщений об ошибках (например, неверное имя пользователя или пароль), а также поля имени пользователя и пароля и кнопку “Войти”.
 -->