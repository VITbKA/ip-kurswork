<?php

/**
 * 1. Включает в себя config.php файл мы создали ранее, так что все параметры конфигурации доступны для скрипта. require() генерирует ошибку, если файл не может быть найден.

 * 2. Эта функция PHP запускает новый сеанс для пользователя, который мы можем использовать для отслеживания того, вошел ли пользователь в систему или нет. (Если сеанс для этого пользователя уже существует, PHP автоматически подбирает его и использует.)

 * Поскольку сеансы нуждаются в куки-файлах для работы, а куки-файлы отправляются в браузер перед контентом, вы должны вызвать session_start() в верхней части скрипта, прежде чем какой-либо контент будет выведен.
 
 * 3. Мы сохраняем параметр $_GET['action'] в переменной $action, а переменную сеанса $_SESSION['username'] в переменной $username, чтобы мы могли использовать эти значения позже в сценарии.

 * Прежде чем сделать это, мы проверяем, что эти значения существуют, используя isset(). Если значение не существует, то мы устанавливаем соответствующую переменную в пустую строку ("").

 * 4. Проверяем $username, чтобы увидеть, содержит ли сеанс значение для ключа username, который мы используем для обозначения того, что пользователь вошел в систему. Если значение $username пустое — и пользователь еще не пытается войти или выйти, — то мы выводим страницу входа и немедленно выходим.

 * 5. Вызывает соответствующую функцию на основе значения параметра action URL. Действие по умолчанию - отображение списка статей в CMS.

 * 6. Вызывается, когда пользователь должен войти в систему или находится в процессе входа в систему.
 ** 6.1. Если пользователь отправил форму входа в систему — которую мы проверяем, ища параметр формы входа в систему, — то функция проверяет введенные имя пользователя и пароль на соответствие конфигурационным значениям ADMIN_USERNAME и ADMIN_PASSWORD. Если они совпадают, то ключ сеанса username устанавливается на имя пользователя admin, эффективно регистрируя их, а затем мы перенаправляем браузер обратно в систему. admin.php скрипт, который затем выводит список статей.
 ** 6.2. Если имя пользователя и пароль не совпадают, то форма входа в систему повторно отображается с сообщением об ошибке.
 ** 6.3. Если пользователь еще не отправил форму входа в систему, то функция просто отображает ее.

 * 7. Функция вызывается, когда пользователь выбирает выход из системы. Он просто удаляет ключ сеанса имени пользователя и перенаправляет его обратно на admin.php.

 * 8. Функция позволяет пользователю создать новую статью. 
 ** 8.1. Если пользователь только что опубликовал форму “новая статья”, то функция создает новый объект статьи, сохраняет данные формы в объекте, вызывая storeFormValues (), вставляет статью в базу данных, вызывая insert (), и перенаправляет обратно в список статей, отображая сообщение о состоянии “изменения сохранены”.
 ** 8.2. Если пользователь еще не разместил форму “новая статья”, то функция создает новый пустой объект статьи без значений, а затем использует editArticle.php шаблон для отображения формы редактирования статьи с использованием этого пустого объекта статьи.

 * 9. Функция аналогична предыдущей функции new Article(), за исключением того, что она позволяет пользователю редактировать существующую статью. Когда пользователь сохраняет свои изменения, функция извлекает существующую статью с помощью getById (), сохраняет новые значения в объекте статьи, а затем сохраняет измененный объект, вызывая update(). (Если статья не найдена в базе данных, то функция выводит сообщение об ошибке.)
 ** 9.1. При отображении формы редактирования статьи функция снова использует метод getById() для загрузки текущих значений полей статьи в форму для редактирования.

 ** Сценарий (template) использует тот же шаблон (editArticle.php) как для создания новых статей, так и для редактирования существующих статей. Это означает, что нам нужно создать только одну HTML-форму. Параметр формирования используется для определения того, добавляет ли пользователь или редактирует статью.

 * 10. Функция сначала извлекает удаляемую статью (выводит ошибку, если статья не может быть найдена в базе данных), а затем вызывает метод delete() статьи, чтобы удалить статью из базы данных. Затем он перенаправляется на страницу списка статей, отображая сообщение о состоянии “статья удалена”.

 * 11. Отображает список всех статей в CMS для редактирования. Эта функция использует метод класса статьи getlist(), чтобы получить все статьи, то он использует шаблон listarticles.php для отображения списка. Попутно он также проверяет параметры URL-запроса error и status, чтобы увидеть, нужно ли отображать на странице какую-либо ошибку или сообщение о состоянии.

 * Если это так, то он создает необходимое сообщение и передает его в шаблон для отображения.

**/

require( "config.php" );                                                  // 1
session_start();                                                          // 2
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";                // 3
$username = isset( $_SESSION['username'] ) ? $_SESSION['username'] : "";

// 4
if ( $action != "login" && $action != "logout" && !$username ) {
  login();
  exit;
}

// 5
switch ( $action ) {
  case 'login':
    login();
    break;
  case 'logout':
    logout();
    break;
  case 'newArticle':
    newArticle();
    break;
  case 'editArticle':
    editArticle();
    break;
  case 'deleteArticle':
    deleteArticle();
    break;
  default:
    listArticles();
}


// 6
function login() {

  $results = array();
  $results['pageTitle'] = "Admin Login | VM News";

  if ( isset( $_POST['login'] ) ) {

    // Пользователь разместил форму входа: попробуйте войти в систему

    if ( $_POST['username'] == ADMIN_USERNAME && $_POST['password'] == ADMIN_PASSWORD ) {

      // Login successful: Create a session and redirect to the admin homepage
      $_SESSION['username'] = ADMIN_USERNAME;
      header( "Location: admin.php" );

    } else {

      // Login failed: display an error message to the user
      $results['errorMessage'] = "Incorrect username or password. Please try again.";
      require( TEMPLATE_PATH . "/admin/loginForm.php" );
    }

  } else {

    // Пользователь еще не разместил форму входа: отобразить форму
    require( TEMPLATE_PATH . "/admin/loginForm.php" );
  }

}


// 7
function logout() {
  unset( $_SESSION['username'] );
  header( "Location: admin.php" );
}


// 8
function newArticle() {

  $results = array();
  $results['pageTitle'] = "New Article";
  $results['formAction'] = "newArticle";

  if ( isset( $_POST['saveChanges'] ) ) {

    // Пользователь разместил форму редактирования статьи: сохраните новую статью
    $article = new Article;
    $article->storeFormValues( $_POST );
    $article->insert();
    header( "Location: admin.php?status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // Пользователь отменил свои правки: вернуться к списку статей
    header( "Location: admin.php" );
  } else {

    // Пользователь еще не разместил форму редактирования статьи: отобразить форму
    $results['article'] = new Article;
    require( TEMPLATE_PATH . "/admin/editArticle.php" );
  }

}


// 9
function editArticle() {

  $results = array();
  $results['pageTitle'] = "Edit Article";
  $results['formAction'] = "editArticle";

  if ( isset( $_POST['saveChanges'] ) ) {

    // Пользователь разместил форму редактирования статьи: сохранить изменения статьи

    if ( !$article = Article::getById( (int)$_POST['articleId'] ) ) {
      header( "Location: admin.php?error=articleNotFound" );
      return;
    }

    $article->storeFormValues( $_POST );
    $article->update();
    header( "Location: admin.php?status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // Пользователь отменил свои правки: вернуться к списку статей
    header( "Location: admin.php" );
  } else {

    // Пользователь еще не разместил форму редактирования статьи: отобразить форму
    $results['article'] = Article::getById( (int)$_GET['articleId'] );
    require( TEMPLATE_PATH . "/admin/editArticle.php" );
  }

}


// 10
function deleteArticle() {

  if ( !$article = Article::getById( (int)$_GET['articleId'] ) ) {
    header( "Location: admin.php?error=articleNotFound" );
    return;
  }

  $article->delete();
  header( "Location: admin.php?status=articleDeleted" );
}


// 11
function listArticles() {
  $results = array();
  $data = Article::getList();
  $results['articles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageTitle'] = "All Articles";

  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "articleNotFound" ) $results['errorMessage'] = "Error: Article not found.";
  }

  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your changes have been saved.";
    if ( $_GET['status'] == "articleDeleted" ) $results['statusMessage'] = "Article deleted.";
  }

  require( TEMPLATE_PATH . "/admin/listArticles.php" );
}

?>
