<?php

/**
 * Класс для обработки статей
 */

class Article
{
  // Свойства

  public $id = null;
  public $publicationDate = null;
  public $title = null;
  public $summary = null;
  public $content = null;

  /**

  * Функция: public function __construct
  * 1. Устанавливает свойства объекта, используя значения в предоставленном массиве

  * 2. Принимает необязательный массив $data, содержащий данные, которые помещаются в свойства нового объекта. Затем мы заполняем эти свойства в теле конструктора. Это дает нам удобный способ создать и заполнить объект за один раз.

  * $ this-> propertyName означает: «свойство этого объекта, имеющего имя« $ propertyName ».
  
  **/

  public function __construct( $data=array() ) {

    /**
    *
    * Этот метод
    * 1. фильтрует данные, прежде чем сохранить их в свойствах.
    
    * Свойства
    * 1. id и pubDate преобразуются в целые числа с помощью функции (int), поскольку эти значения всегда должны быть целыми числами
    * 2. title и summary фильтруются с помощью регулярного выражения, чтобы разрешить только определенный диапазон символов.

    * Если бы мы ограничили диапазон разрешенных символов в контенте, то мы бы ограничили полезность CMS для администратора.

    * Хорошая практика безопасности, чтобы фильтровать данные на ВХОДЕ, как эта, позволяя пропускать только приемлемые значения и символы.
    
    **/

    if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
    if ( isset( $data['publicationDate'] ) ) $this->publicationDate = (int) $data['publicationDate'];
    if ( isset( $data['title'] ) ) $this->title = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['title'] );
    if ( isset( $data['summary'] ) ) $this->summary = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['summary'] );
    if ( isset( $data['content'] ) ) $this->content = $data['content'];
  }


  /**

  * Функция: storeFormValues
  * 1. Устанавливает свойства объекта с помощью формы редактирования post values в предоставленном массиве
  
  * 2. Хранит предоставленный массив данных в свойствах объекта.
  * 3. Может обрабатывать данные в формате, представленном через нашу новую статью, и редактировать формы статей (которые мы создадим позже).

  * Цель метода:
  * Облегчить нашим администраторским скриптам хранение данных, представленных формами. 

  **/

  public function storeFormValues ( $params ) {

    // Store all the parameters
    $this->__construct( $params );

    // Parse and store the publication date
    if ( isset($params['publicationDate']) ) {
      $publicationDate = explode ( '-', $params['publicationDate'] );

      if ( count($publicationDate) == 3 ) {
        list ( $y, $m, $d ) = $publicationDate;
        $this->publicationDate = mktime ( 0, 0, 0, $m, $d, $y );
      }
    }
  }


  /**

  * Функция: getById
  * 1. Возвращает объект статьи, соответствующий указанному идентификатору статьи
  
  * 2. Принимает аргумент идентификатора статьи ($id), затем извлекает запись статьи с этим идентификатором из таблицы статей и сохраняет ее в новом объекте статьи.

  * Чтобы наш метод вызывался без использования объекта, мы добавляем ключевое слово static в определение метода.

  * @return Article|false The article object, or false если запись не была найдена или возникла проблема
  **/

  public static function getById( $id ) {

    /**
    *
    * Свойства
    * 1. Делает соединение с базой данных MySQL, используя регистрационные данные из config.php файл и сохраняет результирующий дескриптор соединения в $pdo. Этот дескриптор используется оставшимся кодом в методе для связи с базой данных.
    
    * 2. Наш оператор SELECT извлекает все поля (*) из записи в таблице articles, которая соответствует заданному полю id. Он также извлекает поле даты публикации в формате UNIX timestamp вместо формата даты MySQL по умолчанию, поэтому мы можем легко хранить его в нашем объекте. :id - placeholder
    
    * 3. Вызывая $pdo->prepare (), сохраняя полученный дескриптор оператора в переменной $st. Подготовленные операторы являются особенностью большинства баз данных; они позволяют вашим вызовам баз данных быть более быстрыми и безопасными.

    * 4. Мы связываем значение нашей переменной $id — то есть идентификатор статьи, которую мы хотим получить — с нашим заполнителем :id, вызывая метод bindValue (). 

    * 5. Вызываем execute() для выполнения запроса, а затем используем fetch() для извлечения результирующей записи в виде ассоциативного массива имен полей и соответствующих значений полей, которые мы храним в переменной $row.

    * 6. Поскольку нам больше не нужно наше соединение, мы закрываем его, присваивая null переменной $pdo. 

    * 7. Последнее, что нужно сделать нашему методу, - это создать новый объект Article, который хранит запись, возвращенную из базы данных, и вернуть этот объект в вызывающий код. Проверяет что из метода fetch(), $row действительно содержит данные. Помним, что это вызывает наш конструктор, который мы создали ранее, который заполняет объект данными, содержащимися в массиве $row.  

    **/

    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );  // 1
    $sql = "SELECT *, UNIX_TIMESTAMP(publicationDate) AS publicationDate FROM articles WHERE id = :id";                           // 2
    $st = $conn->prepare( $sql );                         // 3
    $st->bindValue( ":id", $id, PDO::PARAM_INT );         // 4
    $st->execute();                                       // 5
    $row = $st->fetch();
    $conn = null;                                         // 6
    if ( $row ) return new Article( $row );               // 7
  }


  /**

  * Функция: getList
  * 1. Возвращает все (или диапазон) объекты статьи в БД
  * 2. Может извлекать сразу много статей, а не только 1 статью. Он используется всякий раз, когда нам нужно отобразить список статей пользователю или администратору.

  * @param int Optional The number of rows to return (default=all)
  * @return Array|false A two-element array : results => array, a list of Article objects; totalRows => Total number of articles
  **/

  public static function getList( $numRows=1000000 ) {

    /**
    *
    * Этот метод
    * 1. Принимает необязательный аргумент $numRows: Максимальное количество статей для извлечения. Этот параметр позволяет нам отображать, скажем, только первые 5 статей на главной странице сайта.
    
    * Свойства
    * 1. На этот раз нет предложения WHERE; это происходит потому, что мы хотим получить все статьи, а не статью, которая соответствует определенному идентификатору. 
    ** 1.1. Мы также добавили предложение LIMIT, передающее параметр $numRows (в качестве заполнителя), так что мы можем дополнительно ограничить количество возвращаемых записей. 
    ** 1.2. Специальное значение MySQL SQL_CALC_FOUND_ROWS говорит MySQL возвращать фактическое количество возвращенных записей; эта информация полезна для отображения пользователю, а также для других вещей, таких как разбиение результатов на страницы.

    * 2. Мы создаем массив $list для хранения соответствующих объектов Article. Затем мы используем цикл while для извлечения следующей строки с помощью функции fetch(), создаем новый объект Article, сохраняем значения строк в объекте и добавляем объект в массив $list. Когда строк больше нет, функция fetch() возвращает false и цикл завершается.

    * 3. Чтобы получить количество возвращаемых строк, вычисленных нашей предыдущей командой SQL_CALC_FOUND_ROWS. На этот раз мы используем метод PDO query (), который позволяет быстро выполнить запрос, если нет заполнителей для привязки. Мы вызываем fetch() на результирующем дескрипторе оператора, чтобы получить результирующую строку, а затем возвращаем как список объектов статьи ($list), так и общее количество строк в виде ассоциативного массива.

    **/

    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT SQL_CALC_FOUND_ROWS *, UNIX_TIMESTAMP(publicationDate) AS publicationDate FROM articles
            ORDER BY publicationDate DESC LIMIT :numRows";    // 1

    $st = $conn->prepare( $sql );
    $st->bindValue( ":numRows", $numRows, PDO::PARAM_INT );
    $st->execute();
    $list = array();                                          // 2

    while ( $row = $st->fetch() ) {
      $article = new Article( $row );
      $list[] = $article;
    }

    // 3
    // Получаем общее количество статей, соответствующих критериям.
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalRows = $conn->query( $sql )->fetch();
    $conn = null;
    return ( array ( "results" => $list, "totalRows" => $totalRows[0] ) );
  }


  /**

  * Функция: insert
  * 1. Вставляет текущий объект статьи в базу данных и задает его свойство ID.
  * 2. Добавляет новую запись статьи в таблицу статей, используя значения, хранящиеся в текущем объекте статьи

  * PDO::PARAM_INT при привязке целочисленных значений к заполнителям и PDO::PARAM_STR при привязке строковых значений. Это делается для того, чтобы PDO мог соответствующим образом экранировать значения.

  **/

  public function insert() {

    /**

    * Свойства:
    * 1. Если у него есть идентификатор, то статья, по-видимому, уже существует в базе данных, поэтому мы не должны пытаться вставить ее снова.
    * 2. SQL INSERT для вставки записи в таблицу articles, используя заполнители для передачи значений свойств в базу данных.
    * 3. После выполнения запроса метод извлекает идентификатор новой записи статьи с помощью функции PDO lastInsertId() и сохраняет его в свойстве $id объекта для дальнейшего использования.

    **/

    // 1
    // Does the Article object already have an ID?
    if ( !is_null( $this->id ) ) trigger_error ( "Article::insert(): Attempt to insert an Article object that already has its ID property set (to $this->id).", E_USER_ERROR );

    // 2
    // Insert the Article
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "INSERT INTO articles ( publicationDate, title, summary, content ) VALUES ( FROM_UNIXTIME(:publicationDate), :title, :summary, :content )";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":publicationDate", $this->publicationDate, PDO::PARAM_INT );
    $st->bindValue( ":title", $this->title, PDO::PARAM_STR );
    $st->bindValue( ":summary", $this->summary, PDO::PARAM_STR );
    $st->bindValue( ":content", $this->content, PDO::PARAM_STR );
    $st->execute();
    $this->id = $conn->lastInsertId();  // 3
    $conn = null;
  }


  /**

  * Функция: update
  * 1. Обновляет текущий объект статьи в базе данных.

  **/

  public function update() {

    // Does the Article object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "Article::update(): Attempt to update an Article object that does not have its ID property set.", E_USER_ERROR );
   
    // Update the Article
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "UPDATE articles SET publicationDate=FROM_UNIXTIME(:publicationDate), title=:title, summary=:summary, content=:content WHERE id = :id";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":publicationDate", $this->publicationDate, PDO::PARAM_INT );
    $st->bindValue( ":title", $this->title, PDO::PARAM_STR );
    $st->bindValue( ":summary", $this->summary, PDO::PARAM_STR );
    $st->bindValue( ":content", $this->content, PDO::PARAM_STR );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }


  /**

  * Функция: delete
  * 1. Удаляет текущий объект статьи из базы данных.
  * В целях безопасности мы добавляем ограничение (LIMIT) 1 к запросу, чтобы убедиться, что одновременно можно удалить только 1 запись статьи.
  
  **/

  public function delete() {

    // Does the Article object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "Article::delete(): Attempt to delete an Article object that does not have its ID property set.", E_USER_ERROR );

    // Delete the Article
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $st = $conn->prepare ( "DELETE FROM articles WHERE id = :id LIMIT 1" );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }

}

?>
