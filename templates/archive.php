<?php include "templates/include/header.php" ?>

      <div class="container" id="container">
        <div class="headlines">
          <div class="article">
            <h1 class="article__title">Article Archive</h1>

            <p class="article__total"><span>[ <?php echo $results['totalRows']?> article<?php echo ( $results['totalRows'] != 1 ) ? 's' : '' ?> ]</span> in total.</p>
          </div>
          
          <ul class="headlines__list  archive" id="headlines">

<?php     foreach ( $results['articles'] as $article ) { ?>

            <li class="headlines__item">
              <a class="headlines__link" href=".?action=viewArticle&amp;articleId=<?php echo $article->id?>">
                <span class="pubDate"><?php echo date('j F', $article->publicationDate)?></span>
                <div class="headlines__content">
                  <h3 class="headlines__title"><?php echo htmlspecialchars( $article->title )?></h3>
                  <p class="headlines__summary"><?php echo htmlspecialchars( $article->summary )?></p>
                </div>
              </a>
            </li>

<?php } ?>

          </ul>

          <p class="homepage"><a class="archive-link" href="./">Return to Homepage</a></p>

        </div>
      </div>

<?php include "templates/include/footer.php" ?>

<!-- 
  1. Он добавляет архивный CSS-класс в неупорядоченный список, чтобы мы могли стилизовать элементы списка немного иначе, чем на главной странице, а также добавляет год к датам публикации статьи (так как архив может вернуться на несколько лет назад).

  2. Страница также включает в себя общее количество статей в базе данных, полученное с помощью $results['totalRows']. 
  
  3. Наконец, вместо ссылки на архив в нижней части страницы она включает ссылку “вернуться на главную страницу”.
 -->