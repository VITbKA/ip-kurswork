<?php

/**
 * 1. Включает в себя config.php файл мы создали ранее, так что все параметры конфигурации доступны для скрипта. require() генерирует ошибку, если файл не может быть найден.

 * 2. Мы сохраняем параметр $_GET['action'] в переменной с именем $action, чтобы использовать это значение позже в скрипте. Мы проверяем, что значение $_GET['action'] существует с помощью isset(). Если это не так, мы устанавливаем соответствующую переменную $action в пустую строку ("").
 
 * 3. Блок switch просматривает параметр action в URL-адресе, чтобы определить, какое действие выполнить (отобразить архив или просмотреть статью). Если в URL-адресе нет параметра действия, то скрипт отображает домашнюю страницу сайта.

 * 4. Функция отображает список всех статей в базе данных. Он делает это, вызывая метод getList() класса Article, который мы создали ранее. Затем функция сохраняет результаты вместе с заголовком страницы в ассоциативном массиве $results, чтобы шаблон мог отображать их на странице. Наконец, он включает в себя файл шаблона для отображения страницы. (Сейчас мы создадим шаблоны.)

 * 5. Функция отображает одну страницу статьи. Он извлекает идентификатор статьи для отображения из параметра URL articleId, а затем вызывает метод getById() класса Article для извлечения объекта article, который он хранит в массиве $results для использования шаблоном. (Если articleId не был указан или статья не была найдена, то функция просто отображает домашнюю страницу.)

 * Мы используем (int) для приведения значения параметра запроса article ID к целому числу. Это хорошая мера безопасности

 * 6. Отображает домашнюю страницу сайта, содержащую список статей HOMEPAGE_NUM_ARTICLES (по умолчанию 5).

**/

require( "config.php" );                                      // 1
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";    // 2

switch ( $action ) {                                          // 3
  case 'archive':
    archive();
    break;
  case 'viewArticle':
    viewArticle();
    break;
  default:
    homepage();
}

// 4
function archive() {
  $results = array();
  $data = Article::getList();
  $results['articles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageTitle'] = "Article Archive | VM News";
  require( TEMPLATE_PATH . "/archive.php" );
}

// 5
function viewArticle() {
  if ( !isset($_GET["articleId"]) || !$_GET["articleId"] ) {
    homepage();
    return;
  }

  $results = array();
  $results['article'] = Article::getById( (int)$_GET["articleId"] );
  $results['pageTitle'] = $results['article']->title . " | VM News";
  require( TEMPLATE_PATH . "/viewArticle.php" );
}

// 6
function homepage() {
  $results = array();
  $data = Article::getList( HOMEPAGE_NUM_ARTICLES );
  $results['articles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageTitle'] = "VM News";
  require( TEMPLATE_PATH . "/homepage.php" );
}

?>
