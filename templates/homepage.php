<?php include "templates/include/header.php" ?>

		<div class="container" id="container">
			<div class="headlines">
				<div class="article">
          <h1 class="article__title">Homepage</h1>
        </div>

				<ul class="headlines__list" id="headlines">

<?php 	foreach ( $results['articles'] as $article ) { ?>

	        <li class="headlines__item">
	        	<a class="headlines__link" href=".?action=viewArticle&amp;articleId=<?php echo $article->id?>">
	            <span class="pubDate"><?php echo date('j F', $article->publicationDate)?></span>
	            <div class="headlines__content">
	            	<h3 class="headlines__title"><?php echo htmlspecialchars( $article->title )?></h3>
		          	<p class="headlines__summary"><?php echo htmlspecialchars( $article->summary )?></p>
	            </div>
	        	</a>
	        </li>

<?php } ?>

	      </ul>
			</div>

      <p class="homepage"><a class="archive-link" href="./?action=archive">Article Archive</a></p>
		</div>
      
<?php include "templates/include/footer.php" ?>

<!-- 
	1. Он перебирает массив объектов Article, хранящихся в $results['articles'], и отображает дату публикации, название и резюме каждой статьи. 

	2. Название связано с '.' (index.php), передавая action=viewArticle, а также идентификатор статьи в URL-адресе. Это позволяет посетителю прочитать статью, щелкнув по ее названию.
	
	3. Шаблон также содержит ссылку на архив статьи ("./?action=archive").
	4. Этот шаблон, а также последующие шаблоны используют инструкцию PHP include для включения файлов верхнего и нижнего колонтитулов на странице.
 -->

