<?php include "templates/include/header.php" ?>
			
			<div class="news">
				<div class="container">

					<div class="article">
            <h1 class="article__title">Article <span><?php echo htmlspecialchars( $results['article']->id )?></span></h1>
          </div>

					<h1 class="news__title"><?php echo htmlspecialchars( $results['article']->title )?></h1>
		      <div class="news__summary"><?php echo htmlspecialchars( $results['article']->summary )?></div>
		      <div class="news__content"><?php echo $results['article']->content?></div>
		      <p class="pubDate">Published on <span>[ <?php echo date('j F Y', $results['article']->publicationDate)?> ]</span></p>

		      <p class="homepage"><a class="archive-link" href="./">Return to Homepage</a></p>
				</div>
			</div>
      

<?php include "templates/include/footer.php" ?>

<!-- 
  1. Он отображает название выбранной статьи, резюме и содержание, а также дату ее публикации и ссылку для возврата на главную страницу.
  
  2. Мы не передали $results['article']->content через htmlspecialchars(). Как было объяснено при создании конструктора статьи, администратор, вероятно, захочет использовать HTML-разметку, такую как теги <p>, в содержимом статьи. Если мы закодируем содержимое, то теги <p> будут отображаться на странице как <p>, а не создавать абзацы.
 -->